//
//  unlock2save_iosApp.swift
//  unlock2save-ios
//
//  Created by gil on 01/03/2021.
//

import SwiftUI

@main
struct unlock2save_iosApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
